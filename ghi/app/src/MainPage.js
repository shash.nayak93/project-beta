import lexus from "./pictures/lexus.png"
import ford from "./pictures/ford.png"
import acura from "./pictures/acura.png"

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div id="carouselExampleSlidesOnly" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={lexus} className="d-block w-100" alt="..." />
            </div>
            <div className="carousel-item">
              <img src= {ford} className="d-block w-100" alt="..." />
            </div>
            <div className="carousel-item">
              <img src={acura} className="d-block w-100" alt="..." />
            </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
